import mongoose from 'mongoose';
import ClientAccount from "../models/clientAccount.js";
import dBUtils from "../src/dbUtils.js";


//Get clientAccounts document by ID omitting transanctions
function getClientAccountByIdOmittingTransactions(documentId) {
    ClientAccount.findById({
        "_id": mongoose.Types.ObjectId(dBUtils.hashClientId(documentId))
    }, {
        transactions: 0
    }, (err, results) => {
        if (err) console.log(err.message);
        console.log(results)
    });
};


//Get a specific quantity of transactions from clientAccount document
function findTransactionsByClientAccountId(documentId, quantityOfTransactions) {
    ClientAccount.findById({
            "_id": dBUtils.hashClientId(documentId)
        }).select({
            "transactions": {
                "$slice": quantityOfTransactions
            }
        })
        .exec((err, results) => {
            if (err) console.log(err.message);
            console.log(results)
        })
};

const transactionObject = {
    "transaction_type": "test transaction",
    "reason": "testing",
    "amount": "105",
    "comment": "testing"
};

//Update ClientAccount doc with a new transaction object
function populateTransactions(documentId) {
    ClientAccount.updateOne({
        "_id": dBUtils.hashClientId(documentId)
    }, {
        $push: {
            "transactions": transactionObject
        }
    }, (err, results) => {
        if (err) console.log(err.message);
        console.log(results);
    });
};


function transactionPopulator() {
    var transactionArray = [];
    for (var i = 0; i < 3; i++) {
        transactionArray.push(transactionObject);
    }
    return transactionArray;
};


//Create a new ClientAccount doc and populate it with an array of test transactionObjects
function createNewClientAccountWithASetAmountOfTransactions(newClientAccountObject) {
    var transactionArray = transactionPopulator();
    newClientAccountObject.transactions = transactionArray;
    return dBUtils.createNewClientAccount(newClientAccountObject);
};

export default {
    getClientAccountByIdOmittingTransactions,
    findTransactionsByClientAccountId,
    populateTransactions,
    createNewClientAccountWithASetAmountOfTransactions
};