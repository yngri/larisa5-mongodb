import dbUtils from "../src/dbUtils.js";
import * as express from "express";

const router = express.Router();

router.get("/", (req, res) => {
    res.render("addClient");
});

router.post("/", async (req, res) => {
    try {
        let newClientAccountObject = req.body;
        console.log("addClient form — Received a following client object from req.body: ", newClientAccountObject);
        const responseClientObject = await dbUtils.createNewClientAccount(newClientAccountObject);
        res.send(responseClientObject);
    } catch (error) {
        console.log("Add Client — New client creation: Error ", error);
        res.sendStatus(400);
    };
});

export default router;