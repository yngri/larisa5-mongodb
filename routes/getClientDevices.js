import dbUtils from "../src/dbUtils.js";
import * as express from "express";
import {
    ClientAccount
} from "../models/clientAccount.js";

const router = express.Router();

router.get("/", (req, res) => {
    res.render("getClientDevices");
});

router.get("/:id", async (req, res) => {
    try {
        let clientId = req.params.id;
        if (!clientId.length) {
            console.error("ERROR: getClientDevices form — no client ID entered in the input field")
            return res.status(400).send('No client ID entered in the input field');
        } else {
            console.log("getClientDevices form – received a following ID from req.params.id: ", clientId);
        };
        const responseClientObject = await dbUtils.findClientAccountById(clientId);
        if (responseClientObject == null) {
            console.error("ERROR: getClientDevices form — No records matched the entered Id");
            return res.status(404).send('No records matched the entered Id');
        };
        console.log("getClientDevices form: Received an object from the DB: ", responseClientObject);
        res.send(responseClientObject);
    } catch (error) {
        console.log("Get Client: Error ", error);
    };
});

router.put("/addClientDevice", async (req, res) => {

    console.log("Device Action Form: client ID is: " + req.body.clientId + "; device Tariff is: " + req.body.deviceTariff +
        "; device IMEI is: " + req.body.deviceImei);

    try {
        const result = await ClientAccount.findOneAndUpdate({
            _id: req.body.clientId
        }, {
            $push: {
                [`tariff.${req.body.deviceTariff}`]: req.body.deviceImei
            },
        }, {
            new: true
        });
        
        // function logMapElements(value, key, map) {
        //     console.log(`m[${key}] = ${value}`);
        //   }
          
        //   result.tariff
        //     .forEach(logMapElements);

        // const iterate = (result) => {
        //     Object.keys(result).forEach(key => {

        //         console.log(`key: ${key}, value: ${result[key]}`)

        //         if (typeof result[key] === 'object') {
        //             iterate(result[key])
        //         }
        //     })
        // }

        // console.log("AAAAABBBBBCCCCC: " + iterate)

        res.send(result);
    } catch (error) {
        console.log("Device Action Form — Adding new client device: Error ", error);
        res.sendStatus(400);
    }
});

router.put("/deleteClientDevice", async (req, res) => {

    console.log("Device Action Form: client ID is: " + req.body.clientId + "; device IMEI is: " + req.body.deviceImei);

    try {
        const result = await ClientAccount.updateOne({
            _id: req.body.clientId
        }, {
            $pull: {
                'tariff.210': req.body.deviceImei
            },
        }, {
            new: true
        });
        res.send(result);
    } catch (error) {
        console.log("Device Action Form — Client device deletion: Error ", error);
        res.sendStatus(400);
    }
});

export default router;