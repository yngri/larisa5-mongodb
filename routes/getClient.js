import dbUtils from "../src/dbUtils.js";
import * as express from "express";

const router = express.Router();

router.get("/", (req, res) => {
    res.render("getClient");
});

router.get("/:id", async (req, res) => {
    console.log(req.params.id)
    try {
        let clientId = req.params.id;
        if (!clientId.length) {
            console.error("ERROR: getClient form — no client ID entered in the input field")
            return res.status(400).send('No client ID entered in the input field');
        } else {
            console.log("getClient form – received a following ID from req.params.id: ", clientId);
        };
        const responseClientObject = await dbUtils.findClientAccountById(clientId);
        if (responseClientObject == null) {
            console.error("ERROR: getClient form — No records matched the entered Id");
            return res.status(404).send('No records matched the entered Id');
        };
        console.log("getClient form: Received an object from the DB: ", responseClientObject);
        res.send(responseClientObject);
    } catch (error) {
        console.log("Get Client: Error ", error);
    };
});

export default router;