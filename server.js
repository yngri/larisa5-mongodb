import addClient from "./routes/addClient.js";
import getClient from "./routes/getClient.js";
import getClientDevices from "./routes/getClientDevices.js";

import mongoose from 'mongoose';

const connectionString =
    "mongodb+srv://admin:admin@cluster0-mtnrw.mongodb.net/Larisa5-DEV?retryWrites=true&w=majority";

mongoose.set("useFindAndModify", false);

mongoose
    .connect(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(() => console.log("Connected to MongoDB..."))
    .catch(err => console.error("Could not connect to MongoDB...", err));

import express from "express";
const app = express();

app.set("view engine", "ejs");
app.set("views", "./views");

app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
app.use("/api/addClient", addClient);
app.use("/api/getClient", getClient);
app.use("/api/getClientDevices", getClientDevices);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));