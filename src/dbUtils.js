import {
    ClientAccount
} from "../models/clientAccount.js";
import Dinero from "dinero.js";
import crypto from "crypto";

async function deposit(clientId, increment) {

    try {
        const client = await findClientAccountById(clientId);
        let updatedBalance = incrementDinero(client.balance, increment);
        const filter = {
            _id: hashClientId(clientId)
        };
        const update = {
            balance: {
                amount: updatedBalance.getAmount(),
                currency: updatedBalance.getCurrency()
            }
        };
        var clientAfterUpdate = await ClientAccount.findOneAndUpdate(filter, update, {
            new: true
        });

        console.log("Client record after deposit process", clientAfterUpdate);

    } catch (err) {
        console.log("Error occured during deposit process", err);
    }

};

async function withdraw(clientId, decrement) {

    try {
        const client = await findClientAccountById(clientId);

        let updatedBalance = decrementDinero(client.balance, decrement);

        const filter = {
            _id: hashClientId(clientId)
        };
        const update = {
            balance: {
                amount: updatedBalance.getAmount(),
                currency: updatedBalance.getCurrency()
            }
        };

        var clientAfterUpdate = await ClientAccount.findOneAndUpdate(filter, update, {
            new: true
        });

        console.log("Client record after withdraw process", clientAfterUpdate);

    } catch (err) {
        console.log("Error occured during withdraw process", err);
    }
};

function hashClientId(clientId) {
    return crypto.createHash('shake256', {
            outputLength: 12
        })
        .update(clientId)
        .digest('hex');
}

function incrementDinero(balance, increment) {
    var oldBalance = Dinero({
        amount: balance.amount,
        currency: balance.currency
    });

    return oldBalance.add(Dinero({
        amount: increment,
        currency: balance.currency
    }));
};

function decrementDinero(balance, decrement) {
    var oldBalance = Dinero({
        amount: balance.amount,
        currency: balance.currency
    });

    return oldBalance.subtract(Dinero({
        amount: decrement,
        currency: balance.currency
    }));
};

//Get clientAccounts document by ID
function findClientAccountById(clientId) {
    return ClientAccount.findById(clientId);
};

//Get all documents in a collection
function findAllClientAccounts() {
    return ClientAccount.find();
};

const tariff = new Map();
tariff.set('100', ["123", "234", "345"]);
tariff.set('140', ["456", "567", "678"]);
tariff.set('210', ["567", "678", "789"]);

//console.log(tariff.get('Start'), tariff.get('Pro'));

const newClientAccountObject = {
    "paymentType": "corporate",
    "clientName": "testCompany",
    "edrpo": "321456",
    "corporateRepresentative": "Tariff as Map Test",
    "phoneNumber": "369",
    "messengerType": "viber",
    "email": "tariff@map.com",
    "tariffPlan": "PRO+",
    "tariff": tariff,
    "devicesQuantity": "2",
    "balance": "0",
    "wialonStatus": "active",
    "serverIp": "099"
};

//console.log(newClientAccountObject)


//Create a new ClientAccount document
function createNewClientAccount(newClientAccountObject) {
    return ClientAccount.create({
        "paymentType": newClientAccountObject.paymentType,
        "clientName": newClientAccountObject.clientName,
        "edrpo": newClientAccountObject.edrpo,
        "corporateRepresentative": newClientAccountObject.corporateRepresentative,
        "phoneNumber": newClientAccountObject.phoneNumber,
        "messengerType": newClientAccountObject.messengerType,
        "email": newClientAccountObject.email,
        "tariffPlan": newClientAccountObject.tariffPlan,
        "tariff": newClientAccountObject.tariff,
        "devicesQuantity": newClientAccountObject.devicesQuantity,
        "balance": "0",
        "wialonStatus": newClientAccountObject.wialonStatus,
        "serverIp": newClientAccountObject.serverIp
    });
};

//createNewClientAccount(newClientAccountObject)


//Modify (update) a document by ID
function updateClientAccountById(clientId) {
    return ClientAccount.findByIdAndUpdate(clientId, {
        "tariff": "666",
    }, {
        new: true
    });
};


//Delete a document
function deleteClientAccountById(clientId) {
    return ClientAccount.findByIdAndDelete(clientId);
};


export default {
    hashClientId,
    deposit,
    withdraw,
    incrementDinero,
    decrementDinero,
    findClientAccountById,
    findAllClientAccounts,
    createNewClientAccount,
    updateClientAccountById,
    deleteClientAccountById
};