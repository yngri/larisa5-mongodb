const Dinero = require('dinero.js');

const BalanceUtils = {

    depositMoney: async (clientId, increment) => {
        try {
            var client = await findClientAccountById(clientId);
    
            var balance = Dinero({
                amount: client.balance.amount,
                currency: client.balance.currency
            });
    
            var newBalance = balance.add(Dinero({
                amount: increment,
                currency: client.balance.currency
            }));
    
            const filter = {
                _id: hashFunction(clientId)
            };
            const update = {
                balance: {
                    amount: newBalance.getAmount(),
                    currency: newBalance.getCurrency()
                }
            };
    
            var clientAfterUpdate = await ClientAccountModel.findOneAndUpdate(filter, update, {
                new: true
            });
    
            console.log("Client Record After Update", clientAfterUpdate);
    
        } catch (err) {
            console.log(err);
        }
    },

}

exports.BalanceUtils = BalanceUtils;