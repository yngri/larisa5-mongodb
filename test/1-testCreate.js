import mongoose from 'mongoose';
import dbUtils from "../src/dbUtils.js";
import { resetCounter } from '../models/clientAccount.js';
import { assert } from 'chai';



before(async () => {
    await mongoose.connect("mongodb+srv://admin:admin@cluster0-mtnrw.mongodb.net/Larisa5-DEV?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
    mongoose.connection
        .once("open", () => {
            console.log("Connection to MongoDB established");
        })
        .on("err", error => {
            console.error("Could not connect to MongoDB...", error)
        });
    mongoose.connection.collections.clientAccounts.drop(() => {
        console.log("Collection dropped");
    });
    resetCounter();
});


const clientAccountObject = {
    "paymentType": "corporate",
    "clientName": "testCompany",
    "edrpo": "321456",
    "corporateRepresentative": "Integration Test",
    "phoneNumber": "369",
    "messengerType": "viber",
    "email": "integration@test.com",
    "tariffPlan": "PRO+",
    "tariff": "3",
    "devicesQuantity": "2",
    "balance": "0",
    "wialonStatus": "active",
    "serverIp": "099"
};

describe("Creation test", () => {
    it('should create a new client entry in DB and check if it is saved succesfully', async () => {
        try {
            const creationResult = await dbUtils.createNewClientAccount(clientAccountObject);
            assert.isFalse(creationResult.isNew);
        } catch (error) {
            console.log("CREATION TEST: Error ", error);
            assert(false);
        };
    });
});