import dbUtils from "../src/dbUtils.js";
import { assert } from 'chai';


describe('Read test', () => {
    it("Reads a client doc by ID from DB", async () => {
        try {
            const readResult = await dbUtils.findClientAccountById("10000");
            console.log("User document read from DB: ", readResult);
            assert(readResult != null);
        } catch (error) {
            console.log("READ TEST: No records exist under given ID: ", error);
            assert(false);
        };
    });
});