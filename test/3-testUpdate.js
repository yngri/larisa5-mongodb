import dbUtils from "../src/dbUtils.js";
import { assert } from 'chai';


describe('Update test', () => {
    it("Updates a field in a user document and checks if it was indeed updated", async () => {
        try {
            const updatedDocument = await dbUtils.updateClientAccountById("10000");
            console.log(updatedDocument);
            assert(updatedDocument.tariff == "666");
        } catch (error) {
            console.log("UPDATE TEST: Error ", error);
            assert(false);
        };
    });
});