import dbUtils from "../src/dbUtils.js";
import { assert } from 'chai';


describe("Deletion test", () => {
    it('Deletes a client doc by ID from a DB', async () => {
        try {
            await dbUtils.deleteClientAccountById("10000");
            const searchResult = await dbUtils.findClientAccountById("10000");
            assert(searchResult === null);
        } catch (error) {
            console.log("DELETION TEST: Error ", error);
            assert(false);
        };
    });
});