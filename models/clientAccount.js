import { MongooseAutoIncrementID } from 'mongoose-auto-increment-reworked';
import mongoose from 'mongoose';

const clientAccountSchema = new mongoose.Schema({
  clientName: {
    type: String,
    minlength: 3,
    maxlength: 60,
    required: true,
  },
  edrpo: {
    type: Number,
    maxlength: 25,
  },
  corporateRepresentative: {
    type: String,
    maxlength: 60,
  },
  phoneNumber: {
    type: String,
    required: true,
  },
  messengerToken: {
    type: String,
  },
  messengerType: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  paymentType: {
    type: String,
    required: true,
  },
  tariffPlan: {
    type: String,
    required: true,
  },
  tariff: {
    type: Map,
    of: Array,
  },
  devicesQuantity: {
    type: Number,
    required: true,
  },
  balance: {
    type: Object,
  },
  transactions: {
    type: Array,
    required: true
  },
  wialonStatus: {
    type: String,
    required: true,
  },
  serverIp: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    default: Date.now,
  },
});

clientAccountSchema.plugin(MongooseAutoIncrementID.plugin, {
  modelName: 'ClientAccount',
  incrementBy: 1,
  startAt: 10000
});

const ClientAccount = mongoose.model("ClientAccount", clientAccountSchema, "clientAccounts");

async function resetCounter() {
  await ClientAccount._resetCount()
  .then(val => console.log(`The counter was reset to ${val}`));
};

export {ClientAccount, resetCounter};